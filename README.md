# ナレッジ

ナレッジ共有用

## 岡村を構成している要素達
[岡村wiki](https://gitlab.com/kazuki.okamura.139/front-/wikis/%E5%B2%A1%E6%9D%91%E3%83%8A%E3%83%AC%E3%83%83%E3%82%B8)

### 一般的な知識

> Markdownの書き方忘れたとき用  
https://qiita.com/tbpgr/items/989c6badefff69377da7

> File Typeで制限する際のType指定(Mime Type)  
https://developer.mozilla.org/ja/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types  

> console.log系  
https://qiita.com/mtoyopet/items/7274761af5424cee342a

> VSCodeの便利機能  
https://qiita.com/12345/items/64f4372fbca041e949d0

> つまずいたらここ  
https://stackoverflow.com/  
https://qiita.com/

### JS系

> await/catch  
https://qiita.com/Nkzn/items/fb729bccf78a880084d8

> Next.js Docs  
https://nextjs.org/docs

> React.js Docs  
https://devdocs.io/react/  
https://ja.reactjs.org/docs/getting-started.html

> Material UI Docs  
https://material-ui.com/getting-started/installation/

> 基本的なJS関数  
https://developer.mozilla.org/ja/

### デザイン思想系

> Atomic Design 
http://atomicdesign.bradfrost.com/table-of-contents/  
https://www.indetail.co.jp/blog/10234/  

> Atomic Designでのコンポーネント設計  
https://blog.spacemarket.com/code/atomic-design%E3%82%92%E4%BD%BF%E3%81%A3%E3%81%A6react%E3%82%B3%E3%83%B3%E3%83%9D%E3%83%BC%E3%83%8D%E3%83%B3%E3%83%88%E3%82%92%E5%86%8D%E8%A8%AD%E8%A8%88%E3%81%97%E3%81%9F%E8%A9%B1/

> React + Atomic相性  
https://note.mu/konojunya/n/nc720dd77d0be  

> Atomic Design - molecules/organisms  
https://frasco.io/atomic-design-molecules-organisms-dc937b5989

### DB系

> GraphQL Docs  
https://graphql.org/learn/

> Graph QLについて  
https://employment.en-japan.com/engineerhub/entry/2018/12/26/103000

> Elastic Search  
https://qiita.com/nskydiving/items/1c2dc4e0b9c98d164329  

### Framework/libalies系  

> Quarkus Java  
https://quarkus.io/

> Spring  
https://spring.io/

> Springの脆弱性  
https://www.jpcert.or.jp/at/2018/at180014.html

### Docker系

> Docker Docs  
https://docs.docker.com/get-started/